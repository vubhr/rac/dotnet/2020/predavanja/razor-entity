using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;

namespace Notes.Pages.Notes {
  public class IndexModel : PageModel {
    private readonly NotesDbContext db;

    public IndexModel(NotesDbContext db) {
      this.db = db;
    }

    public void OnGet() {
      Notes = db.Notes.ToList();
    }

    public IEnumerable<Note> Notes { get; set; }
  }
}
